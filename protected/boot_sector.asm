[org 0x7c00]

xor ax, ax
mov es, ax
mov ds, ax

mov bp, 0x9000
mov ss, ax
mov sp, bp

mov bx, MSG_REAL_MODE
call print_string

call switch_to_pm

jmp $

%include "print_string.asm"
%include "gdt.asm"
%include "print_string_pm.asm"
%include "switch_to_pm.asm"

[bits 32]
;Where we arrive after switching to PM
BEGIN_PM:
    mov ebx, MSG_PROTECTED_MODE
    call print_string_pm

    jmp $


MSG_REAL_MODE: db "16-bit real mode.", 0
MSG_PROTECTED_MODE: db "Successfully landed in 32-bit protected mode.", 0

times 510-($-$$) db 0
dw 0xaa55
