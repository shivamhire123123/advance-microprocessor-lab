org 0x8000
mov ah, 0x04
mov al, 0x08
mov si, M_G
mov di, M_T
call print_matrix
call print_nl
call matrix_transpose
mov si, di
mov ah, 0x08
mov al, 0x04
call print_matrix

jmp $   ; this freezes the system, best for testing
hlt		;this makes a real system halt
ret     ;this makes qemu halt, to ensure everything works we add both


matrix_transpose:
	pusha
	movq mm1, [si]
	movq mm2, [si + 8]
	movq mm3, [si + 16]
	movq mm4, [si + 24]
	punpcklbw mm1, mm2
	punpcklbw mm3, mm4
	movq mm0, mm1
	punpcklwd mm1, mm3
	punpckhwd mm0, mm3
	movq [di], mm1
	movq [di + 8], mm0
	movq mm1, [si]
	movq mm3, [si + 16]
	punpckhbw mm1, mm2
	punpckhbw mm3, mm4
	movq mm0, mm1
	punpcklwd mm1, mm3
	punpckhwd mm0, mm3
	movq [di + 16], mm1
	movq [di + 24], mm0
	popa
	ret

; following code prints a A X B matrix
; ah <- A
; al <- B
; si <- address of matrix
print_matrix:
	pusha
	mov bx, ax
	mov ch, 0x00
	mov cl, bh
	print_matrix_row:
		jcxz print_matrix_row_end
		push cx
		mov cl, bl
		print_matrix_col:
			jcxz print_matrix_col_end
			mov al, [si]
			call put_number
			call print_space
			inc si
			dec cx
			jmp print_matrix_col
		print_matrix_col_end:
		pop cx
		dec cx
		call print_nl
		jmp print_matrix_row
	print_matrix_row_end:
	popa
	ret

; following code prints a number
; al <- the number to be printed
put_number:
	pusha
	mov ah, 0x00
	mov bl, 0x0a
	div bl
	test al, al
	jz put_number_end
	call put_number
	put_number_end:
		mov al, ah
		call put_digit
		popa
		ret
put_digit:
	pusha
	add al, 0x30
	call printCharacter
	popa
	ret
printCharacter:
	;before calling this function al must be set to the character to print
	mov bh, 0x00 ;page to write to, page 0 is displayed by default
	mov bl, 0x00 ;color attribute, doesn't matter for now
	mov ah, 0x0E 
	int 0x10 ; int 0x10, 0x0E = print character in al
	ret	
msg db "Hello World!"

print_space:
	push ax
	push bx
	mov al, ' '
	mov bh, 0x00 ;page to write to, page 0 is displayed by default
	mov bl, 0x00 ;color attribute, doesn't matter for now
	mov ah, 0x0E
	int 0x10 ; int 0x10, 0x0E = print character in al
	pop bx
	pop ax
	ret

print_nl:
	push ax
	push bx
	mov al, 0x0a
	mov bh, 0x00 ;page to write to, page 0 is displayed by default
	mov bl, 0x00 ;color attribute, doesn't matter for now
	mov ah, 0x0E
	int 0x10 ; int 0x10, 0x0E = print character in al
	mov al, 0x0d
	mov ah, 0x0E
	int 0x10 ; int 0x10, 0x0E = print character in al
	pop bx
	pop ax
	ret

M_G db 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07
db 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
db 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17
db 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f

M_T db 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
db 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
db 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
db 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
