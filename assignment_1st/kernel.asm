org 0x8000 
bits 16
mov si, msg
call printString

jmp $
hlt
ret

printCharacter:
	;before calling this function al must be set to the character to print
	mov bh, 0x00
	mov bl, 0x00
	mov ah, 0x0E
	int 0x10
	ret
printString:
	pusha
	.loop:
		lodsb
		test al, al
		jz .end
		call printCharacter
	jmp .loop
	.end:
	popa
	ret
msg db "Hello World"
times 512-($-$$) db 0
